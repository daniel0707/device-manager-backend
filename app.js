const express = require("express")
const logger = require("morgan")
const app = express()
const cors = require("cors")
const apiRouter = require("./routes/api")
const path = require("path")

app.use(logger("dev"))
app.use(cors())
app.use("/deviceapi", apiRouter)

app.use("/",express.static("public"))
app.get("/*", function (req, res) {
	res.sendFile(path.join(__dirname, "public", "index.html"))
})
module.exports = app
