const Devices = require("../models/device")

//use case 2
const getDevice = async (id) => {
	if (id) {
		const d = await Devices.get(id)
		if (d) {
			return d.toJSON()
		}
	}
	return null
}

// use case 1
const fetchDevices = async (limit) => {
	const d = await Devices.scan().limit(limit).exec()
	if (d) {
		return d.toJSON()
	}else return null
}

//use case 3
const createDevice = async (device) => {
	try {
		const d = new Devices({
			"EquipmentNumber": Number(device.EquipmentNumber),
			"Address": device.Address,
			"ContractStartDate": Date.parse(device.ContractStartDate) ,
			"ContractEndDate": Date.parse(device.ContractEndDate),
			"Status": Boolean(device.Status) 
		})
		const savedDevice = await d.save({ overwrite: false })
		if (savedDevice) {
			return savedDevice.toJSON()
		}
	} catch (e) {
		return null
	}
}

module.exports = {
	create: createDevice,
	get: getDevice,
	fetch: fetchDevices
}