const dynamoose = require("dynamoose")

const deviceSchema = new dynamoose.Schema({
	"EquipmentNumber": Number,
	"Address": String,
	"ContractStartDate": Date,
	"ContractEndDate": Date,
	"Status": Boolean
})

module.exports = dynamoose.model("devices", deviceSchema,{create: false, waitForActive: false})