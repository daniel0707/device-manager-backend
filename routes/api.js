const express = require("express")
const apiRouter = express.Router()
const devices = require("../controllers/devices")

apiRouter.use(express.json())

apiRouter.get("/search", async (req, res) => {
	const limit = req.query.limit? Number(req.query.limit) || 100 : 100
	const d = await devices.fetch(limit)
	if (d) {
		res.json(d)
	} else {
		res.status(404).send()
	}
})

apiRouter.get("/:id", async (req, res) => {
	const d = await devices.get(Number(req.params.id))
	if (d) {
		res.json(d)
	} else {
		res.status(404).send()
	}
})

apiRouter.post("/", async(req,res)=> {
	const d = await devices.create(req.body)
	if (d) {
		res.json(d)
	} else {
		res.status(400).send()
	}
})

module.exports = apiRouter